# Waiters

`WakeKey` – ключ, адресующий ядерную очередь ожидания, в реализации – просто адрес 32-битной ячейки памяти.

Предложим другой угол зрения: `PrepareWake` возвращает _view очереди ожидания_, 
над которой возможны две операции – `WakeOne` / `WakeAll`.

Если вам близка такая интерпретация, то подходящим именем для возвращаемого
`PrepareWake` значения будет `waiters` или `wait_queue`:

```cpp
// examples/event

void OneShotEvent::Fire() {
  auto waiters = futex_like::PrepareWake(fired_);
  fired_.store(1, std::memory_order::release);
  futex_like::WakeAll(waiters);
}
```
