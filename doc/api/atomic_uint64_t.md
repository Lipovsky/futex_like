# `atomic_uint64_t`

Функции `Wait`, `WaitOnce`, `WaitTimed` и `PrepareWake` могут работать не только с `std::atomic_uint32_t`,
но и отдельно с младшей / старшей половиной `std::atomic_uint64_t`.

Ссылку на нужную половину `std::atomic_uint64_t` нужно получить с помощью функций `RefLowHalf` / `RefHighHalf`.

Тип ссылки:
- `AtomicUint64LowHalfRef` для `RefLowHalf`
- `AtomicUint64HighHalfRef` для `RefHighHalf`

Ссылки – _trivially copyable_.

Вызов `Wait` на половине 64-битного атомика возвращает его **полное** значение.

См. https://lkml.org/lkml/2008/5/31/2

## Пример

[examples/atomic_uint64_t](/examples/atomic_uint64_t/main.cpp)

```cpp
// Будем использовать старшие биты state для хранения флага
std::atomic_uint64_t state{0};

const uint64_t kSetFlag = (uint64_t)1 << 32;

// Ссылка для ожидания
auto flag_ref = futex_like::RefHighHalf(state);

std::thread t([&, flag_ref] {
  auto wake_key = futex_like::PrepareWake(flag_ref);
  state.fetch_or(kSetFlag);
  futex_like::WakeOne(wake_key);
});

// Wait возвращает новое значение атомика (целиком)
uint64_t s = futex_like::Wait(flag_ref, 0);

auto f = s >> 32;
assert(f == 1);

t.join();
```
