# Базовые операции

Зафиксируем `std::atomic<uint32_t>`, операции поддержаны только для него
(и для половин `std::atomic<uint64_t>`, см. [отдельный пункт](atomic_uint64_t.md)).

## Обозначения

Через `SysWait` и `SysWake` обозначим системные вызовы для парковки / пробуждения потока в данной операционной системе.

(например, для Linux – системный вызов [`futex`](https://man7.org/linux/man-pages/man2/futex.2.html) и операции `FUTEX_WAIT` / `FUTEX_WAKE` соответственно)

Через `Loc(atom)` – адрес атомика `atom` в памяти.

## Wait

### `Wait`

```cpp
uint32_t Wait(std::atomic<uint32_t>& atom, uint32_t old, std::memory_order mo = std::memory_order::seq_cst) {
  uint32_t curr;
  
  do {
    SysWait(Loc(atom), old);
  } while ((curr = atom.load(mo)) == old);
  
  return curr;  // Новое значение
}
```

`Wait` возвращает управление только когда он прочел из атомика новое (отличное от `old`) значение.

### `WaitOnce`

```cpp
void WaitOnce(std::atomic<uint32_t>& atom, uint32_t old) {
  SysWait(Loc(atom), old);
}
```

`WaitOnce` подвержен spurious wakeups.

## Wake

Протокол `Wake` – двухфазный:

1) Сначала (**до** записи в атомик, которая предшествует пробуждению) с помощью `PrepareWake` фиксируется ключ (`WakeKey`) для адресации системной очереди ожидания, связанной с атомиком (фактически – адрес атомика в памяти):

```cpp
WakeKey PrepareWake(std::atomic<uint32_t>& atom) {
  return {Loc(atom)};
}
````

`WakeKey` – _trivially copyable_

2) Затем (**после** записи в атомик) вызывается `Wake{One,All}` с адресом, зафиксированном на первом шаге, этот вызов будит ждущие потоки.

```cpp
void WakeOne(WakeKey key) {
  SysWake(key.loc, 1);
}
```

Обратите внимание: `PrepareWake` следует выполнять **до записи** в атомик.
