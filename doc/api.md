# API

Заголовок: [`futex_like/wait_wake.hpp`](/source/futex_like/wait_wake.hpp) \
Пространство имен: `futex_like::`

- [Базовые операции](api/wait_wake.md) 
- [Ожидание с таймаутом](api/wait_timed.md)
- [Ожидание на половинах `atomic_uint64_t`](api/atomic_uint64_t.md)