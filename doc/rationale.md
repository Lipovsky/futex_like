# Rationale

Библиотека предлагает API ожидания для `std::atomic`, альтернативный стандартному API – методам [`wait`](https://en.cppreference.com/w/cpp/atomic/atomic/wait) / `notify_{one, all}`.

[Проблемы API ожидания `std::atomic`](why_not_atomic_wait.md)

Библиотека 

1) ограничивается поддержкой ожидания для `std::atomic_uint32_t`,
2) поддерживает ожидание на половинах `std::atomic_uint64_t`,
3) использует аккуратный двухфазный протокол пробуждения.
