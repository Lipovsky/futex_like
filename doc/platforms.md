# Платформы

- Linux / `futex`: [doc](https://man7.org/linux/man-pages/man7/futex.7.html), [src](https://github.com/torvalds/linux/blob/master/kernel/futex/waitwake.c)
- Darwin / `ulock_wait`: [src](https://github.com/apple-oss-distributions/xnu/blob/main/bsd/kern/sys_ulock.c)
- [TODO] Windows / `WaitOnAddress`: [doc](https://learn.microsoft.com/en-us/windows/win32/api/synchapi/nf-synchapi-waitonaddress)
