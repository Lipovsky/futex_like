# Futex-_Like_

Thread parking for C++ atomics done [_right_](doc/rationale.md).

- [Example](examples/event/one_shot_event.hpp)
- [API](doc/api.md)
