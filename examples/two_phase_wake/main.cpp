#include <futex_like/wait_wake.hpp>

#include <atomic>
#include <optional>
#include <thread>

// https://wg21.link/p2616

int main() {
  std::optional<std::thread> t;

  {
    std::atomic_uint32_t flag{0};

    t.emplace([&] {
      auto waiters = futex_like::PrepareWake(flag);
      flag.store(1);  // 1
      futex_like::WakeOne(waiters);  // 4
    });

    futex_like::Wait(flag, 0); // 2
    assert(flag.load() == 1);
  };  // ~flag // 3

  t->join();

  return 0;
}
