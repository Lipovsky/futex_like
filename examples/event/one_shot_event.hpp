#pragma once

#include <futex_like/wait_wake.hpp>

#include <atomic>

class OneShotEvent {
 public:
  void Wait() {
    while (!fired_.load(std::memory_order::acquire)) {
      futex_like::WaitOnce(fired_, /*old=*/0);
    }
  }

  void Fire() {
    auto wake_key = futex_like::PrepareWake(fired_);
    fired_.store(1, std::memory_order::release);
    futex_like::WakeAll(wake_key);
  }

 private:
  std::atomic<uint32_t> fired_{0};
};
