# Run

```shell
git clone https://gitlab.com/Lipovsky/futex_like.git
cd futex_like
mkdir -p build && cd build
cmake -DCMAKE_CXX_COMPILER=/usr/bin/clang++-19 -DFUTEX_LIKE_EXAMPLES=ON ..
make futex_like_examples_event
./examples/event/futex_like_examples_event
```
