#include "one_shot_event.hpp"

#include <chrono>
#include <thread>
#include <iostream>

using namespace std::chrono_literals;

int main() {
  int data;
  OneShotEvent ready;

  std::thread producer([&] {
    std::this_thread::sleep_for(1s);
    data = 42;
    ready.Fire();
  });

  ready.Wait();
  std::cout << data << std::endl;  // Prints 42

  producer.join();

  return 0;
}
