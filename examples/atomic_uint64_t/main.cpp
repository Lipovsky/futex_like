#include <futex_like/wait_wake.hpp>

#include <chrono>
#include <thread>

using namespace std::chrono_literals;

int main() {
  std::atomic_uint64_t state{0};

  const uint64_t kSetFlag = (uint64_t)1 << 32;

  // Ссылка для ожидания
  auto flag_ref = futex_like::RefHighHalf(state);

  std::thread t([&, flag_ref] {
    std::this_thread::sleep_for(1s);

    auto wake_key = futex_like::PrepareWake(flag_ref);
    state.fetch_or(kSetFlag);
    futex_like::WakeOne(wake_key);
  });

  uint64_t s = futex_like::Wait(flag_ref, 0);

  auto f = s >> 32;
  assert(f == 1);

  t.join();

  return 0;
}
