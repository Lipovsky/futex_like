#pragma once

#include <futex_like/system/wait.hpp>

#include <cassert>

namespace futex_like {

namespace atomic {

template <typename AtomicRef>
void WaitOnce(AtomicRef atom_ref, uint32_t old) {
  system::Wait(atom_ref.FutexLoc(), old);
}

template <typename AtomicRef>
typename AtomicRef::AtomicValueType Wait(AtomicRef atom_ref, uint32_t old,
                                         std::memory_order mo) {
  typename AtomicRef::AtomicValueType atom_curr_value;

  do {
    system::Wait(atom_ref.FutexLoc(), old);
    atom_curr_value = atom_ref.AtomicLoad(mo);
  } while (AtomicRef::GetFutexValue(atom_curr_value) == old);

  assert(AtomicRef::GetFutexValue(atom_curr_value) != old);

  return atom_curr_value;  // New value
}

}  // namespace atomic

}  // namespace futex_like
